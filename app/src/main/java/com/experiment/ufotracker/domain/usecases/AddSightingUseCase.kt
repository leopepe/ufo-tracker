package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.entities.Sighting

interface AddSightingUseCase {

    operator fun invoke(sighting: Sighting)
}
