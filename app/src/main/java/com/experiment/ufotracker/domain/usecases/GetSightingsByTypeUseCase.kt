package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import kotlinx.coroutines.flow.Flow

interface GetSightingsByTypeUseCase {

    operator fun invoke(typeList: List<SightingType>): Flow<List<Sighting>>
}
