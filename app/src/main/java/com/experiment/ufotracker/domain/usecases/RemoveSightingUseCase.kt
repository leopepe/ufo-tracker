package com.experiment.ufotracker.domain.usecases

interface RemoveSightingUseCase {

    operator fun invoke(id: Int)
}
