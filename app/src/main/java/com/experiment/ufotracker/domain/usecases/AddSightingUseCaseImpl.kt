package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.repositories.SightingsRepository
import javax.inject.Inject

class AddSightingUseCaseImpl @Inject constructor(
    private val sightingsRepository: SightingsRepository
) : AddSightingUseCase {

    override fun invoke(sighting: Sighting) {
        sightingsRepository.addSighting(sighting)
    }
}
