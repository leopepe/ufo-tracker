package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import com.experiment.ufotracker.data.repositories.SightingsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetSightingsByTypeUseCaseImpl @Inject constructor(
    private val sightingsRepository: SightingsRepository
) : GetSightingsByTypeUseCase {
    override operator fun invoke(typeList: List<SightingType>): Flow<List<Sighting>> {
        return flow {
            sightingsRepository.getAllSightings().collect {
                emit(it.filter { sighting -> sighting.sightingType in typeList })
            }
        }
    }
}
