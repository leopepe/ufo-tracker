package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.repositories.SightingsRepository
import javax.inject.Inject

class RemoveSightingUseCaseImpl @Inject constructor(
    private val sightingsRepository: SightingsRepository
) : RemoveSightingUseCase {

    override fun invoke(id: Int) {
        sightingsRepository.removeSighting(id)
    }
}
