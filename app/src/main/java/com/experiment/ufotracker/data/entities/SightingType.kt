package com.experiment.ufotracker.data.entities

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class SightingType : Parcelable {
    BLOB, LAMPSHADE, MYSTERIOUS_LIGHTS
}
