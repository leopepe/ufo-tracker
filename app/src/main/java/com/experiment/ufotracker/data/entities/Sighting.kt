package com.experiment.ufotracker.data.entities

data class Sighting(
    val id: Int,
    val dateOfSighting: String,
    val sightingType: SightingType,
    val hourOfSighting: String,
    val speed: Int
)
