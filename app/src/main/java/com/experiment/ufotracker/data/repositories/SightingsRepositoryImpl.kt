package com.experiment.ufotracker.data.repositories

import com.experiment.ufotracker.data.entities.Sighting
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class SightingsRepositoryImpl @Inject constructor() : SightingsRepository {

    private val sightingList: MutableList<Sighting> = mutableListOf()

    override fun getAllSightings(): Flow<List<Sighting>> {
        return flowOf(sightingList)
    }

    override fun addSighting(sighting: Sighting) {
        val newSighting = if (sightingList.isEmpty()) {
            sighting.copy(id = sighting.id)
        }
        else {
            sighting.copy(
                id = sightingList.maxOf {
                    it.id
                } + 1
            )
        }
        sightingList.add(newSighting)
    }

    override fun removeSighting(id: Int) {
        sightingList.removeAt(sightingList.indexOfFirst {
            it.id == id
        })
    }
}
