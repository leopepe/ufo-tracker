package com.experiment.ufotracker.data.repositories

import com.experiment.ufotracker.data.entities.Sighting
import kotlinx.coroutines.flow.Flow

interface SightingsRepository {

    fun getAllSightings(): Flow<List<Sighting>>

    fun addSighting(sighting: Sighting)

    fun removeSighting(id: Int)
}
