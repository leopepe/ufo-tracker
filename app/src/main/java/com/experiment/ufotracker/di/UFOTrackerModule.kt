package com.experiment.ufotracker.di

import com.experiment.ufotracker.data.repositories.SightingsRepository
import com.experiment.ufotracker.data.repositories.SightingsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UFOTrackerModule {

    @Singleton
    @Provides
    fun providesSightingsRepository(): SightingsRepository {
        return SightingsRepositoryImpl()
    }
}
