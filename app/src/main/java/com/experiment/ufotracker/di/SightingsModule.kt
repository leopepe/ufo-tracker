package com.experiment.ufotracker.di

import com.experiment.ufotracker.domain.usecases.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class SightingsModule {

    @Binds
    abstract fun providesGetSightingByTypeUseCase(
        getSightingsByTypeUseCase: GetSightingsByTypeUseCaseImpl
    ): GetSightingsByTypeUseCase

    @Binds
    abstract fun providesAddSightingUseCase(
        addSightingUseCase: AddSightingUseCaseImpl
    ): AddSightingUseCase
    
    @Binds
    abstract fun providesRemoveSightingUseCase(
        removeSightingUseCase: RemoveSightingUseCaseImpl
    ): RemoveSightingUseCase
}
