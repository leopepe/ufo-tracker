package com.experiment.ufotracker

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class UFOTrackerApplication : Application()
