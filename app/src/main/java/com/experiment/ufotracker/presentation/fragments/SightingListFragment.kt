package com.experiment.ufotracker.presentation.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.experiment.ufotracker.R
import com.experiment.ufotracker.data.entities.SightingType
import com.experiment.ufotracker.databinding.FragmentSightingListBinding
import com.experiment.ufotracker.presentation.adapters.SightingAdapter
import com.experiment.ufotracker.presentation.util.SIGHTING_TYPES_BUNDLE_KEY
import com.experiment.ufotracker.presentation.viewmodels.SightingListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SightingListFragment : Fragment() {

    private val viewModel: SightingListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentSightingListBinding.inflate(inflater, container, false)

        val adapter = SightingAdapter(this::removeSighting)

        binding.recyclerView.adapter = adapter

        val sightingTypes: List<SightingType> = arguments?.getParcelableArray(
            SIGHTING_TYPES_BUNDLE_KEY
        )?.map { it as SightingType } ?: emptyList()

        viewModel.sightingListLiveData.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
        viewModel.setSightingTypes(sightingTypes)
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.add_sighting -> {
                viewModel.addNewSighting()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun removeSighting(id: Int) {
        viewModel.removeSighting(id)
    }
}
