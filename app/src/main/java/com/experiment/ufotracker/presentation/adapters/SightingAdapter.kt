package com.experiment.ufotracker.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.experiment.ufotracker.R
import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import com.experiment.ufotracker.databinding.LayoutItemSightingBinding

class SightingAdapter(
    private val removeSightingListener: RemoveSightingListener
) : ListAdapter<Sighting, SightingViewHolder>(SightingDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SightingViewHolder {
        return SightingViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: SightingViewHolder, position: Int) {
        holder.bind(getItem(position), removeSightingListener)
    }
}

class SightingViewHolder(
    private val binding: LayoutItemSightingBinding
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun from(parent: ViewGroup): SightingViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = LayoutItemSightingBinding.inflate(layoutInflater, parent, false)
            return SightingViewHolder(binding)
        }
    }

    fun bind(sighting: Sighting, removeSightingListener: RemoveSightingListener) {
        val context = binding.root.context
        val sightingTypeName = when (sighting.sightingType) {
            SightingType.BLOB -> {
                context.getString(R.string.blob)
            }
            SightingType.LAMPSHADE -> {
                context.getString(R.string.lamp_shade)
            }
            SightingType.MYSTERIOUS_LIGHTS -> {
                context.getString(R.string.mysterious_lights)
            }
        }
        binding.textViewSightingTitle.text = context.getString(
            R.string.sighting_title,
            sighting.dateOfSighting,
            sighting.hourOfSighting
        )
        binding.textViewSightingDetails.text = context.getString(
            R.string.sighting_details,
            context.resources.getQuantityString(
                R.plurals.speedInKnots,
                sighting.speed,
                sighting.speed
            ),
            sightingTypeName
        )
        binding.imageViewSightingIcon.apply {
            contentDescription = sightingTypeName
            when (sighting.sightingType) {
                SightingType.BLOB -> {
                    setImageResource(R.drawable.ic_blob)
                }
                SightingType.LAMPSHADE -> {
                    setImageResource(R.drawable.ic_ufo)
                }
                SightingType.MYSTERIOUS_LIGHTS -> {
                    setImageResource(R.drawable.ic_lights)
                }
            }
        }
        binding.imageViewDeleteIcon.visibility = View.GONE
        binding.root.setOnLongClickListener {
            when (binding.imageViewDeleteIcon.visibility) {
                View.VISIBLE -> binding.imageViewDeleteIcon.visibility = View.GONE
                else -> binding.imageViewDeleteIcon.visibility = View.VISIBLE
            }
            return@setOnLongClickListener true
        }
        binding.imageViewDeleteIcon.setOnClickListener {
            removeSightingListener(sighting.id)
        }
    }
}

class SightingDiffUtil : DiffUtil.ItemCallback<Sighting>() {
    override fun areItemsTheSame(oldItem: Sighting, newItem: Sighting): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Sighting, newItem: Sighting): Boolean {
        return oldItem == newItem
    }
}

typealias RemoveSightingListener = (Int) -> Unit
