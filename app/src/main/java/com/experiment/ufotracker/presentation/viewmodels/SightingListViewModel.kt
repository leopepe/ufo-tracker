package com.experiment.ufotracker.presentation.viewmodels

import androidx.lifecycle.*
import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import com.experiment.ufotracker.domain.usecases.AddSightingUseCase
import com.experiment.ufotracker.domain.usecases.GetSightingsByTypeUseCase
import com.experiment.ufotracker.domain.usecases.RemoveSightingUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SightingListViewModel @Inject constructor(
    private val getSightingsByTypeUseCase: GetSightingsByTypeUseCase,
    private val addSightingUseCase: AddSightingUseCase,
    private val removeSightingUseCase: RemoveSightingUseCase
) : ViewModel() {

    private val _sightingTypes: MutableLiveData<List<SightingType>> = MutableLiveData()

    private val _sightingListLiveData: MutableLiveData<List<Sighting>> = MutableLiveData()
    val sightingListLiveData: LiveData<List<Sighting>> = _sightingListLiveData

    fun setSightingTypes(sightingTypes: List<SightingType>) {
        _sightingTypes.value = sightingTypes
        refreshSightings()
    }

    private fun refreshSightings() {
        viewModelScope.launch {
            getSightingsByTypeUseCase(_sightingTypes.value ?: emptyList()).collect { sightingList ->
                _sightingListLiveData.postValue(sightingList)
            }
        }
    }

    fun addNewSighting() {
        val newSighting = Sighting(
            0,
            mockDates.random(),
            _sightingTypes.value?.random() ?: SightingType.MYSTERIOUS_LIGHTS,
            mockHours.random(),
            (1..10).random()
        )
        addSightingUseCase(newSighting)
        refreshSightings()
    }

    fun removeSighting(id: Int) {
        removeSightingUseCase(id)
        refreshSightings()
    }

    private val mockDates = listOf(
        "January 13, 2000",
        "February 20, 1997",
        "March 03, 2001",
        "April 27, 1997",
        "May 17, 1997"
    )

    private val mockHours = listOf(
        "4:20 PM",
        "3:56 AM",
        "2:37 PM",
        "11:58 PM",
        "3:33 AM"
    )
}
