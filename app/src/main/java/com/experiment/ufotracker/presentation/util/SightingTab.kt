package com.experiment.ufotracker.presentation.util

import com.experiment.ufotracker.data.entities.SightingType

data class SightingTab(
    val tabName: String,
    val sightingTypes: List<SightingType>
)

val sightingTabs = listOf(
    SightingTab(
        "STRANGE FLYERS",
        listOf(SightingType.LAMPSHADE, SightingType.BLOB)
    ),
    SightingTab(
        "MYSTERIOUS LIGHTS",
        listOf(SightingType.MYSTERIOUS_LIGHTS)
    )
)
