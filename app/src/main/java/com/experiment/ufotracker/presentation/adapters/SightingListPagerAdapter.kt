package com.experiment.ufotracker.presentation.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.experiment.ufotracker.presentation.fragments.SightingListFragment
import com.experiment.ufotracker.presentation.util.SIGHTING_TYPES_BUNDLE_KEY
import com.experiment.ufotracker.presentation.util.sightingTabs

class SightingListPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return sightingTabs.size
    }

    override fun createFragment(position: Int): Fragment {
        val newSightingListFragment = SightingListFragment()
        newSightingListFragment.arguments = Bundle().apply {
            putParcelableArray(
                SIGHTING_TYPES_BUNDLE_KEY,
                sightingTabs[position].sightingTypes.toTypedArray()
            )
        }
        return newSightingListFragment
    }
}
