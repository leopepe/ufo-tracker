package com.experiment.ufotracker.presentation.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.experiment.ufotracker.databinding.FragmentUfoTrackerHomeBinding
import com.experiment.ufotracker.presentation.adapters.SightingListPagerAdapter
import com.experiment.ufotracker.presentation.util.sightingTabs
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UFOTrackerHomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentUfoTrackerHomeBinding.inflate(inflater, container, false)

        val adapter = SightingListPagerAdapter(this)
        binding.viewPager.adapter = adapter

        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = sightingTabs[position].tabName
        }.attach()

        return binding.root
    }
}
