package com.experiment.ufotracker.data.repositories

import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import com.google.common.truth.Truth.assertThat

@ExperimentalCoroutinesApi
class SightingsRepositoryImplTest {

    private val sightingsRepository = SightingsRepositoryImpl()

    @Test
    fun `Test that getAllSightings returns a Flow with an empty list of sightings`() =
        runBlockingTest {

            val sightingsFlow = sightingsRepository.getAllSightings()
            val sightingsList = sightingsFlow.first()

            assertThat(sightingsList).isEmpty()
        }

    @Test
    fun `Test that addSighting adds a sighting to the list of sightings`() = runBlockingTest {

        sightingsRepository.addSighting(getMockedSighting())
        val sightingsList = sightingsRepository.getAllSightings().first()

        assertThat(sightingsList).containsExactly(getMockedSighting())
    }

    @Test
    fun `Test that removeSighting removes an existing sighting in the list of sightings`() =
        runBlockingTest {
            sightingsRepository.addSighting(getMockedSighting())

            val sightingsList = sightingsRepository.getAllSightings().first()
            sightingsRepository.removeSighting(getMockedSighting().id)

            assertThat(sightingsList).isEmpty()
        }

    private fun getMockedSighting(): Sighting {
        return Sighting(
            1,
            "",
            SightingType.BLOB,
            "",
            1
        )
    }
}
