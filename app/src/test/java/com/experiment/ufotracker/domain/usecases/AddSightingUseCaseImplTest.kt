package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import com.experiment.ufotracker.data.repositories.SightingsRepository
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

class AddSightingUseCaseImplTest {

    private val sightingsRepository: SightingsRepository = mock()
    private val addSightingUseCaseImpl = AddSightingUseCaseImpl(sightingsRepository)

    @Test
    fun `Test that invoking Use Case calls the repository`() {

        addSightingUseCaseImpl(getMockedSighting())

        verify(sightingsRepository, times(1)).addSighting(getMockedSighting())
    }

    private fun getMockedSighting(): Sighting {
        return Sighting(
            1,
            "",
            SightingType.BLOB,
            "",
            1
        )
    }
}
