package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import com.experiment.ufotracker.data.repositories.SightingsRepository
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

class RemoveSightingUseCaseImplTest {

    private val sightingsRepository: SightingsRepository = mock()
    private val removeSightingUseCaseImpl = RemoveSightingUseCaseImpl(sightingsRepository)

    @Test
    fun `Test that invoking Use Case calls the repository`() {

        removeSightingUseCaseImpl(getMockedSighting().id)

        verify(sightingsRepository, times(1)).removeSighting(getMockedSighting().id)
    }

    private fun getMockedSighting(): Sighting {
        return Sighting(
            1,
            "",
            SightingType.BLOB,
            "",
            1
        )
    }
}
