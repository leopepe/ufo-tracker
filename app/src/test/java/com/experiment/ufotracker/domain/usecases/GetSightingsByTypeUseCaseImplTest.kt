package com.experiment.ufotracker.domain.usecases

import com.experiment.ufotracker.data.entities.Sighting
import com.experiment.ufotracker.data.entities.SightingType
import com.experiment.ufotracker.data.repositories.SightingsRepository
import kotlinx.coroutines.flow.flow
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest

@ExperimentalCoroutinesApi
class GetSightingsByTypeUseCaseImplTest {

    private val sightingsRepository: SightingsRepository = mock()
    private val getSightingsByTypeUseCaseImpl = GetSightingsByTypeUseCaseImpl(sightingsRepository)

    @Test
    fun `Test that invoking Use Case returns filtered Sightings`() = runBlockingTest {
        whenever(sightingsRepository.getAllSightings()).thenReturn(
            flow {
                emit(getFakeSightingsList())
            }
        )

        val filteredList = getSightingsByTypeUseCaseImpl(listOf(SightingType.BLOB)).first()

        assertThat(filteredList).containsExactlyElementsIn(
            getFakeSightingsList().filter { it.sightingType == SightingType.BLOB }
        )
    }

    private fun getFakeSightingsList(): List<Sighting> {
        return listOf(
            Sighting(
                1,
                "",
                SightingType.LAMPSHADE,
                "",
                1
            ),
            Sighting(
                2,
                "",
                SightingType.BLOB,
                "",
                1
            ),
            Sighting(
                3,
                "",
                SightingType.MYSTERIOUS_LIGHTS,
                "",
                1
            ),
            Sighting(
                4,
                "",
                SightingType.BLOB,
                "",
                1
            )
        )
    }
}
