# Readme

## Notes about the code base

So I tried to focus the code on being as scalable as possible within the timeframe I was given, this includes separating the concerns in what is usually found in projects that follow the called _Clean Architecture_. For example, I divided the package structure in four main packages:
- Data: Here I included the entities and repositories that I was going to use, in this case I had the `Sighting` and `SightingRepository`.
- Domain: Here I included everything related to the Use Cases, otherwise known as Interactors, the most notorious example is the `GetSightingsByType`  which gets all the `Sighting`s from the repository and filters them by the passed list of types.
- Presentation: Here lies all the code related to android, including Activities, Fragments, ViewModels, Adapters etc.
- DI: The simplest package to include `Hilt` dependency injection

## Notes about copied-in code

I think the only copied-in code I used is the setup code for the navigation component:
```
val navHostFragment =  
    supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment  
val navController = navHostFragment.navController  
setupActionBarWithNavController(  
    navController,  
  AppBarConfiguration(setOf(R.id.UFOTrackerHomeFragment))  
)
```
And core `build.gradle` files for both project and application module which include my always used dependencies.

## How long I spent in the project

So I think I spent well over 3 hours on this project, this might be more than the expected time but I did take some snack-breaks. Overall I could've saved time if I had kept the list of sightings on a per-ViewModel basis but ended up implementing a Repository alongside it's UseCases so that required a bit more time than I initially expected, especially because I had never used `ViewPager2` which was needed to implement the tab control for the categories.

## Something extra I'd like you to know!

I had a blast working on this project, I learned a lot about `ViewPager2` and also I decided to go with kotlin's `Flow` for the communication between `ViewModels`-`UseCases`-`Repositories` which was also relatively new for me, so I'd totally love to get some feedback on how I could further improve this!